-r common.txt

django-debug-toolbar==1.10.1
django-rosetta==0.9.0
Faker
flake8
flake8-print
flake8-comprehensions
pytest
pytest-django
mock
pyinotify
