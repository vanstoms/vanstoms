from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.utils import timezone
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings

from apps.api.v1.permissions import ObjectPermissions
from apps.api.v1.serializers import BadRequestResponseSerializer, CommonErrorResponseSerializer
from apps.api.v1.serializers.accounts import (
    UserChangePasswordSerializer,
    UserFullSerializer,
    UserLoginResponseSerializer,
    UserRegistrationSerializer,
)
from apps.api.v1.tokens import EmailVerifyTokenGenerator, PasswordResetTokenGenerator
from apps.api.v1.viewsets import ExtendedModelViewSet
from apps.notifications.tasks import send_templated_email

User = get_user_model()
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


class UserViewSet(ExtendedModelViewSet):
    """
    View for working with user data.

    retrieve:
    Return the given user.

    list:
    List all users.

    update:
    Update user data.

    partial_update:
    Partial update user data.

    delete:
    Mark user as deleted.
    """

    queryset = User.objects.all()
    serializer_class = UserFullSerializer
    serializer_class_map = {
        'login': JSONWebTokenSerializer,
        'registration': UserRegistrationSerializer,
        'change_password': UserChangePasswordSerializer,
    }
    permission_map = {
        'login': permissions.AllowAny,
        'registration': permissions.AllowAny,
        'verify_email': permissions.AllowAny,
        'send_verify_email': permissions.AllowAny,
    }
    permission_classes = (ObjectPermissions,)

    @swagger_auto_schema(
        responses={
            200: UserLoginResponseSerializer,
            400: BadRequestResponseSerializer,
            410: BadRequestResponseSerializer,
        }
    )
    @action(methods=['post'], detail=False)
    def login(self, request):
        """
        Retrieve auth token by pair of username & password.
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(response_data)
            if api_settings.JWT_AUTH_COOKIE:
                expiration = timezone.now() + api_settings.JWT_EXPIRATION_DELTA
                response.set_cookie(api_settings.JWT_AUTH_COOKIE, token, expires=expiration, httponly=True)
            return response

    @swagger_auto_schema(responses={201: UserFullSerializer, 400: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False)
    def registration(self, request):
        """
        Register user with first_name, last_name, email, phone, password1, password2 fields.
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            self._send_verify_email(request, user)
            data = UserFullSerializer(instance=user).data
            return Response(data, status=status.HTTP_201_CREATED)

    @action(methods=['get'], detail=False)
    def me(self, request, pk=None, **kwargs):
        """
        Retrieve logged user information.
        """
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)

    @swagger_auto_schema(
        responses={
            200: UserChangePasswordSerializer,
            400: BadRequestResponseSerializer,
            410: BadRequestResponseSerializer,
        }
    )
    @action(methods=['post'], detail=False, url_path='change-password')
    def change_password(self, request):
        """
        Retrieve auth token by pair of email & password or phone & password.
        """
        serializer = self.get_serializer(data=request.data, instance=request.user)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            data = UserFullSerializer(instance=request.user).data
            return Response(data)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['get'], detail=True, url_path=r'verify-email/(?P<key>[^/.]+)')
    def verify_email(self, request, pk=None, key=None, **kwargs):
        """
        Verify user email.
        """
        user = self.get_object()
        is_valid_token = EmailVerifyTokenGenerator().check_token(user, key)
        if is_valid_token:
            user.is_email_proved = True
            user.save()
            return Response()
        else:
            return Response({'errors': {'token': _('Not valid email token')}}, status=status.HTTP_400_BAD_REQUEST)

    def _send_verify_email(self, request, user):
        key = EmailVerifyTokenGenerator().make_token(user)
        refferer = request.META.get('HTTP_HOST')
        # TODO: Need specify uri!
        path = reverse('api_v1:api-root:accounts-verify-email', kwargs={'pk': user.id, 'key': key})
        url = f'https://{refferer}{path}'
        send_templated_email.delay(_('Verify your email'), user.email, 'notifications/verify_email.html', url=url)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(
        methods=['get'], detail=False, url_path=r'send-verify-email/(?P<email>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})'
    )
    def send_verify_email(self, request, email=None, **kwargs):
        """
        Send verify user email.
        """
        try:
            user = User.objects.get(email=email)
        except User.DoesNotExist:
            data = {'errors': {'email': _('user not registered')}}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        else:
            self._send_verify_email(request, user)
            return Response()
