from rest_framework import viewsets

from apps.api.v1.permissions import ObjectPermissions
from apps.api.v1.serializers.protections import BuildingSerializer, DoorSerializer, RoomSerializer
from apps.protections.models import Building, Door, Room


class BuildingViewSet(viewsets.ModelViewSet):
    queryset = Building.objects.all()
    serializer_class = BuildingSerializer
    permission_classes = (ObjectPermissions,)


class DoorViewSet(viewsets.ModelViewSet):
    queryset = Door.objects.all()
    serializer_class = DoorSerializer
    permission_classes = (ObjectPermissions,)


class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = (ObjectPermissions,)
