import pytest
import string
from random import choices, shuffle


from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.core.cache import cache
from django.urls import reverse
from faker import Faker
from guardian.shortcuts import assign_perm
from mock import MagicMock, Mock
from rest_framework import status
from rest_framework.test import APITestCase, force_authenticate
from django.db.models.query import QuerySet


User = get_user_model()
fake = Faker()
# pytestmark = pytest.mark.django_db


def _pseudo_random_phone():
    last4nums = choices(string.digits, k=4)
    shuffle(last4nums)
    return "+7927640" + "".join(last4nums)


def _create_user(**kwargs):
    perms = kwargs.pop("perms", None)
    password = kwargs.pop("password", None)
    profile = fake.profile()
    username = profile["username"]
    first_name = profile["name"].split()[1]
    last_name = profile["name"].split()[0]
    email = fake.email()
    phone = _pseudo_random_phone()
    data = {"username": username, "first_name": first_name, "last_name": last_name, "phone": phone, "email": email}
    data.update(kwargs)
    user = User.objects.create(**data)
    if password:
        user.set_password(password)
        user.save()
    if perms:
        for perm in perms:
            assign_perm(perm, user)
    return user


# @pytest.mark.django_db
class AccountTests(APITestCase):
    # pytestmark = pytest.mark.django_db

    # def setUp(self):
    #     self.user = _create_user(is_active=True, is_staff=True, is_superuser=True)

    # @pytest.mark.django_db
    def test_register_user(self):
        first_name = fake.first_name()
        last_name = fake.last_name()
        email = fake.email()
        phone = "+79276400000"
        password1 = fake.password()
        password2 = password1

        data = {
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "phone": phone,
            "password1": password1,
            "password2": password2,
            "is_agree_with_agreement": True,
        }

        url = reverse("api_v1:api-root:accounts-registration")
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)  # first is guardian AnonymousUser
        self.assertEqual(response.data["first_name"], first_name)
        self.assertEqual(response.data["last_name"], last_name)
        self.assertEqual(response.data["email"], email)
        self.assertEqual(response.data["phone"], phone)

        user = User.objects.get(id=response.data["id"])

        self.assertTrue(user.is_active)
        self.assertFalse(user.is_superuser)
        self.assertFalse(user.is_staff)
