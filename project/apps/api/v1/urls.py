from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions, routers
from apps.api.v1.views.accounts import UserViewSet
from apps.api.v1.views.protections import BuildingViewSet, DoorViewSet, RoomViewSet

router = routers.DefaultRouter()
router.register(r'accounts', UserViewSet, basename='accounts')
router.register(r'buildings', BuildingViewSet, basename='buildings')
router.register(r'doors', DoorViewSet, basename='doors')
router.register(r'rooms', RoomViewSet, basename='rooms')

schema_view = get_schema_view(
    openapi.Info(title='Vanstoms API', default_version='v1', description='Routes of Vanstoms project'),
    # validators=['flex', 'ssv'],
    public=False,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('swagger(<str:format>.json|.yaml)', schema_view.without_ui(), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger'), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc'), name='schema-redoc'),
    path('', include((router.urls, 'api-root')), name='api-root'),
]
