from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.validators import UniqueValidator, ValidationError

User = get_user_model()


class UserFullSerializer(serializers.ModelSerializer):
    phone = PhoneNumberField(required=False)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'middle_name', 'last_name', 'birthday', 'gender', 'phone', 'email', 'photo')
        read_only_fields = ('id', 'phone', 'email', 'photo')


class UserLoginResponseSerializer(serializers.Serializer):
    token = serializers.CharField()


class UserRegistrationSerializer(serializers.Serializer):
    # TODO: Move max [first|last]_name length to settings.
    first_name = serializers.CharField(validators=[MaxLengthValidator(30)])
    middle_name = serializers.CharField(validators=[MaxLengthValidator(30)], required=False)
    last_name = serializers.CharField(validators=[MaxLengthValidator(150)])
    email = serializers.EmailField(validators=[UniqueValidator(User.objects.all())], required=False)
    phone = PhoneNumberField(validators=[UniqueValidator(User.objects.all())], required=False)
    is_agree_with_agreement = serializers.NullBooleanField()

    password1 = serializers.CharField()
    password2 = serializers.CharField()

    def validate_password2(self, value):
        validate_password(value)
        return value

    def validate(self, attrs):
        if attrs['password1'] != attrs['password2']:
            raise ValidationError(detail=_('Passwords does not match'))
        return super().validate(attrs)

    def create(self, validated_data):
        username = validated_data.get('email')
        is_agree_with_agreement = validated_data.get('is_agree_with_agreement')
        user = User.objects.create(
            username=username,
            first_name=validated_data.get('first_name'),
            last_name=validated_data.get('last_name'),
            email=validated_data.get('email'),
            phone=validated_data.get('phone'),
            is_agree_with_agreement=timezone.now() if is_agree_with_agreement else None,
        )
        user.set_password(validated_data.get('password2'))
        user.save()
        return user


class UserChangePasswordSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)
    password1 = serializers.CharField(required=True)
    password2 = serializers.CharField(required=True)

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()
        return self.instance

    def validate_password(self, value):
        if not self.instance.check_password(value):
            raise ValidationError(detail=_('Passwords does not correct'))
        return value

    def validate_password1(self, value):
        validate_password(value)
        return value

    def validate_password2(self, value):
        validate_password(value)
        return value

    def validate(self, attrs):
        if attrs['password1'] != attrs['password2']:
            raise ValidationError(detail=_('Passwords does not match'))
        return super().validate(attrs)
