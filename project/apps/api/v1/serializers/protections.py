from rest_framework import serializers

from apps.protections.models import Building, Door, Room


class BuildingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Building
        fields = ('id', 'name')
        read_only_fields = ('id',)


class DoorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Door
        fields = ('id', 'name', 'source', 'target', 'two_way')
        read_only_fields = ('id',)


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ('id', 'name', 'building')
        read_only_fields = ('id',)
