from django.contrib.auth import models as auth_models
from django.contrib.auth.models import Permission
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField
from versatileimagefield.fields import VersatileImageField

from apps.helpers.models import UserDeletedModel, UUIDModel


class User(UserDeletedModel, auth_models.AbstractUser):
    NOT_DEFINED = 'n'
    MALE = 'm'
    FEMALE = 'f'
    GENDER_CHOICES = ((NOT_DEFINED, _('not defined')), (MALE, _('male')), (FEMALE, _('female')))

    first_name = models.CharField(_('first name'), max_length=30)
    middle_name = models.CharField(_('middle name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150)
    birthday = models.DateField(_('birthday'), null=True, blank=True)
    gender = models.CharField(_('gender'), max_length=1, choices=GENDER_CHOICES, default=NOT_DEFINED)
    phone = PhoneNumberField(_('phone'), null=True, blank=True, unique=True)
    email = models.EmailField(_('email address'), unique=True, null=True)
    photo = VersatileImageField(_('photo'), upload_to='user_photos', null=True, blank=True)

    is_agree_with_agreement = models.DateTimeField(_('is agree with user agreement'), null=True, blank=True)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta(auth_models.AbstractUser.Meta):
        ordering = ('email',)
        swappable = 'AUTH_USER_MODEL'
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        indexes = (models.Index(fields=('email',)),)
