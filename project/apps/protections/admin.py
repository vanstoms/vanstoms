from django.contrib import admin

from apps.protections.models import Building, Room, Door


@admin.register(Building)
class BuildingAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ('name',)


@admin.register(Door)
class DoorAdmin(admin.ModelAdmin):
    list_display = ('name',)

