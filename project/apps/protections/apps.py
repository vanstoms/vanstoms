from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ProtectionsConfig(AppConfig):
    name = 'apps.protections'
    verbose_name = _('protections')
