from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.helpers.models import CreatedDeletedModel, UUIDModel


class Building(CreatedDeletedModel, UUIDModel):
    name = models.CharField(_('Name'), max_length=126)

    class Meta:
        verbose_name = _('Building')
        verbose_name_plural = _('Buildings')


class Door(CreatedDeletedModel, UUIDModel):
    name = models.CharField(_('Name'), max_length=126)
    source = models.ForeignKey(
        'Room', related_name='sources', verbose_name=_('Source'), blank=True, null=True, on_delete=models.CASCADE
    )
    target = models.ForeignKey(
        'Room', related_name='targets', verbose_name=_('Target'), blank=True, null=True, on_delete=models.CASCADE
    )
    two_way = models.BooleanField(_('Two way access'), default=True)

    class Meta:
        verbose_name = _('Door')
        verbose_name_plural = _('Doors')
        permissions = (('access_source', 'Can access to source'), ('access_target', 'Can access to target'))


class Room(CreatedDeletedModel, UUIDModel):
    name = models.CharField(_('Name'), max_length=126)
    building = models.ForeignKey(Building, related_name='rooms', verbose_name=_('Building'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Room')
        verbose_name_plural = _('Rooms')

