from django.core.mail import EmailMessage
from django.template.loader import get_template
from twilio.rest import Client
from django.conf import settings

from apps import app


@app.task
def send_templated_email(subject, to_email, template_name, **kwargs):
    html = get_template(template_name).render(kwargs)
    email = EmailMessage(subject, html, to=[to_email])
    email.content_subtype = 'html'
    email.send()


@app.task
def send_sms(to_number, from_number, body):
    client = Client(settings.TWILIO_ACCOUNT, settings.TWILIO_TOKEN)
    client.messages.create(to=to_number, from_=from_number, body=body)


@app.task
def send_templated_sms(to_number, template_name, **kwargs):
    body = get_template(template_name).render(kwargs)
    client = Client(settings.TWILIO_ACCOUNT, settings.TWILIO_TOKEN)
    client.messages.create(to=to_number, from_=settings.TWILIO_DEFAULT_FROM, body=body)
