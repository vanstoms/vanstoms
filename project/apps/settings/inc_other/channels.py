from apps.settings.common import env


ASGI_APPLICATION = 'apps.routing.application'

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": env.cache('CHANNEL_BROKERS', default='redis://redis/2'),
        },
    },
}